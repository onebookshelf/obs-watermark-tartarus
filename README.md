# Watermark Tartarus

Welcome to Tartarus.  This repo contains copies of PDF files that have; for one reason and another, been difficult to process or caused/revealed errors in our PDFlib powered watermarking software in the past.  As we encounter these types of isses we hold on to them so that they can be used for future testing.  It is - in a sense - a rogues gallery of problem PDFs.  

You will require [GIT-LFS](https://git-lfs.github.com/) in order to work with this repository.  

## Naming Conventions

Each file in the repo is labeled (by the filename) as to the nature of the offence that led to its consignment to the depths of Tartarus.  The first part of a filename  
is always a **prefix** which denotes the classification of problem.  This is followed by an **underscore** and then a *dash-separated* explanation of the specific problem  
or test case that the file covers.  The current **prefixes** in are use are:  

+ `baseline_*` — These are files that failed to watermark in the past for various reasons and are now part of the baseline test suite (usually that means they are malformed, difficult to process, or we just don’t remember why they were originally added)

+ `feature_*` — These are files that have specific features (or specific traits of specific features) that we want to test for coverage

+ `integration_*` — These are files that combine groups of features that we want to test all together

## Adding Files

Before adding a file to Tartarus ensure that [Watermark](https://bitbucket.org/onebookshelf/obs-watermark) test suites can pass with this new file.  If not you will need to update or repair  
the code so that the case is covered and add test coverage if the case this PDF exposes is not currently tested by the existing tests.
